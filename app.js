const fastify = require('fastify')({ logger: true });


var students = new Map();


fastify.get('/', (req, reply) => {
  reply.send("Server working, so ready to do tasks.");
});


fastify.post('/add', (req, reply) => {
  if (students[req.body.studentID] != undefined)
    reply.send(`${req.body.studentName} ${req.body.studentID} details already exists!.`);
 
  students[req.body.studentID] = req.body;
  reply.send(`${req.body.studentName} ${req.body.studentID} details has been added!.`);
});


fastify.post('/update', (req, reply) => {
  if (students[req.body.studentID] != undefined) {
    for (let key in req.body) {
      students[req.body.studentID][key] = req.body[key];
    } 
    reply.send(`${req.body.studentName} mark is/are updated in the system!.`);
  }  
  reply.send(`No records for student ID:${req.body.studentID} Found to update!.`);
});


fastify.delete('/delete', (req, reply) => {
  if (students[req.body.studentID] != undefined){
    delete students[req.body.studentID];
    reply.send(`Student ID: ${req.body.studentID} details has been deleted!.`);
  } 
  reply.send( `No records for student ID:${req.body.studentID} Found to delete!.`);
});


fastify.get('/report', (req, reply) => {
  reply.send(students);
});


const PORT = 5000
var start = async () => {
  try {
    await fastify.listen(PORT)
  } 
  catch (error) {
    fastify.log.error(error)
    process.exit(1)
  }
}
start();